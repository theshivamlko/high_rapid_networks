
import 'package:flutter/material.dart';

Color primaryColor=const Color(0xFF5282F0);
Color txtColor1=const Color(0xFF5282F0);
Color btnColor=const Color(0xFF5282F0);

String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
    "\\@" +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
    "(" +
    "\\." +
    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
    ")+";