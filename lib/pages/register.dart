import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';
import 'serviceAddress.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RegisterPageStateFull();
  }
}

class RegisterPageStateFull extends StatefulWidget {
  RegisterPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  _RegisterPageState createState() => new _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPageStateFull> {
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Register"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Container(
                    color: Colors.white,
                    child: Stack(
                      children: <Widget>[
                        ListView(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 100.0)),
                            Text(
                              "General Information",
                              style:
                                  TextStyle(color: txtColor1, fontSize: 18.0),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Username',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Email',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  labelText: 'Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      print('111');
                                    },
                                    child: _obscureText
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  labelText: 'Confirm Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: _obscureText
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Checkbox(value: _isChecked, onChanged: (value) {
                                  setState(() {
                                    _isChecked=!_isChecked;
                                  });
                                }),
                                Text(
                                  'I agree with ',
                                  style: TextStyle(
                                    color: txtColor1,
                                  ),
                                ),
                                Text(
                                  " Terms and Conditions",
                                  style: TextStyle(
                                      color: btnColor,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: SizedBox(
                                    height: 50.0,
                                    width: 350.0,
                                    child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(8.0),
                                      ),
                                      color: btnColor,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ServiceAddressPageStateFull()),
                                        );
                                      },
                                      child: Text('Register'),
                                    ))),
                            addBottomText()
                          ],
                        )
                      ],
                    ),
                  ))),
        ));
    ;
  }

  Widget addBottomText() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RegisterPageStateFull()),
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Already have an account?',
                    style: TextStyle(
                      color: txtColor1,
                    ),
                  ),
                  Text(
                    " Login?",
                    style: TextStyle(
                        color: Colors.green.shade800,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              )),
        ));
  }
}
