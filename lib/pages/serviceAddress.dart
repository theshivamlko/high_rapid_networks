import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';

class ServiceAddressPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ServiceAddressPageStateFull();
  }
}

class ServiceAddressPageStateFull extends StatefulWidget {
  ServiceAddressPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  ServiceAddressPageState createState() => new ServiceAddressPageState();
}

class ServiceAddressPageState extends State<ServiceAddressPageStateFull> {
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  bool _isChecked = false;
  List<DropdownMenuItem<int>> dropList = [];

  loadData() {
    dropList.add(DropdownMenuItem(
      child: Text('Select City'),
      value: 1,
    ));
    dropList.add(DropdownMenuItem(
      child: Text('Gurgaon'),
      value: 2,
    ));
    dropList.add(DropdownMenuItem(
      child: Text('Lucknow'),
      value: 3,
    ));
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Request for Service"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Container(
                      color: Colors.white,
                      child: SingleChildScrollView(
                          child: Column(
                         children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 100.0)),
                          Text(
                            "Service Address Information",
                            style: TextStyle(color: txtColor1, fontSize: 18.0),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: 'First Name',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: 'Last Name',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                                labelText: 'Phone Number',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                                labelText: 'E-Mail Address',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                                labelText: 'Street Address',
                                labelStyle: TextStyle(fontSize: 18.0),
                                fillColor: Colors.red,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          SizedBox(
                            width:double.infinity,
                            child: DropdownButton(
                                items: dropList,
                                hint: Text('Select City'),
                                onChanged: null),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          SizedBox(
                            width:double.infinity,
                            child: Container(child:DropdownButton(
                                items: dropList,
                                hint: Text('Select States'),
                                onChanged: null)
                            , decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blueAccent)
                              ),),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            maxLength: 6,
                            decoration: InputDecoration(
                                labelText: 'Zip Code',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 30.0)),
                          TextFormField(
                            keyboardType: TextInputType.text,
                            maxLines: 4,
                            textAlign: TextAlign.start,
                            decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 10.0),
                                labelText: 'Comments/Driving Directions',
                                hintText:
                                    'Comments and/or driving directions to your location',
                                labelStyle: TextStyle(fontSize: 18.0),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius
                                        .all(Radius.circular(5.0)))),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: SizedBox(
                                  height: 50.0,
                                  width: 350.0,
                                  child: RaisedButton(
                                    shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                    ),
                                    color: btnColor,
                                    textColor: Colors.white,
                                    onPressed: () {},
                                    child: Text('Finish'),
                                  ))),
                        ],
                      ))))),
        ));
  }
}
