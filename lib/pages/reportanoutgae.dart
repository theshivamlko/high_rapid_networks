import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';
import '../widget/toolbar.dart';
import 'serviceAddress.dart';

class ReportOutagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReportOutagePageStateFull();
  }
}

class ReportOutagePageStateFull extends StatefulWidget {
  ReportOutagePageStateFull({
    Key key,
  }) : super(key: key);

  @override
  _ReportOutagePageState createState() => new _ReportOutagePageState();
}

class _ReportOutagePageState extends State<ReportOutagePageStateFull> {
  List<DropdownMenuItem<int>> list = [];

  loadData() {
    list.add(DropdownMenuItem(
      child: Text('Select'),
      value: 1,
    ));
    list.add(DropdownMenuItem(
      child: Text('David'),
      value: 2,
    ));
    list.add(DropdownMenuItem(
      child: Text('John'),
      value: 3,
    ));
  }

  @override
  Widget build(BuildContext context) {
    loadData();

    return Material(
        color: Colors.white,
        child: Container(
            child: Column(
          children: <Widget>[
            addToolbar('3456', '9876'),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            Padding(
              padding: EdgeInsets.only(left: 50.0, right: 50.0),
              child: Column(children: <Widget>[
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'FRIEND CONTACT',
                    style: TextStyle(fontSize: 16.0, color: txtColor1),
                    textAlign: TextAlign.start,
                  ),
                ),
                SizedBox(
                  width: double.infinity,
                  child: DropdownButton(
                      items: list, hint: Text('Select'), onChanged: null),
                ),
                Padding(padding: EdgeInsets.only(top: 30.0)),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'CONTACT METHOD',
                    style: TextStyle(fontSize: 16.0, color: txtColor1),
                    textAlign: TextAlign.start,
                  ),
                ),
                TextField(
                  keyboardType: TextInputType.emailAddress,
                  maxLines: 4,
                  decoration: InputDecoration(
                      hintText: 'Start typing...',
                      labelStyle: TextStyle(fontSize: 18.0),
                      border: OutlineInputBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(5.0)))),
                ),
                Padding(padding: EdgeInsets.only(top: 50.0)),
                SizedBox(
                    height: 50.0,
                    width: 350.0,
                    child: RaisedButton(
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      color: Colors.green,
                      textColor: Colors.white,
                      onPressed: () {},
                      child: Text('Register your friend'),
                    )),
              ]),
            ),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            SizedBox(
                height: 50.0,
                width: 350.0,
                child: RaisedButton(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),
                  ),
                  color: btnColor,
                  textColor: Colors.white,
                  onPressed: () {},
                  child: Text('Cancel'),
                ))
          ],
        )));
  }

  Widget addBottomText() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: GestureDetector(
              onTap: () {},
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Already have an account?',
                    style: TextStyle(
                      color: txtColor1,
                    ),
                  ),
                  Text(
                    " Login?",
                    style: TextStyle(
                        color: Colors.green.shade800,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              )),
        ));
  }
}
