import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class BillingListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.red,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryTextTheme:
            Theme.of(context).primaryTextTheme.apply(bodyColor: Colors.black87),
        accentColor: Colors.amber,
        primaryColor: Colors.black87,
        primaryIconTheme:
            Theme.of(context).primaryIconTheme.copyWith(color: Colors.black87),

        /*  textTheme: Theme.of(context).textTheme.apply(
            bodyColor: Colors.red,
            displayColor: Colors.green,
          )*/
      ),
      home: new CommentWidgetState(),
    );
  }
}

class CommentWidgetState extends StatefulWidget {
  @override
  _CommentWidgetStateState createState() => _CommentWidgetStateState();
}

class _CommentWidgetStateState extends State<CommentWidgetState> {
  List<String> data;

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          title: Text(
            'Account #4432   Account Balance \$550',
            style: TextStyle(color: Colors.blueAccent,fontSize: 16.0),
          ),
          backgroundColor: Colors.white,
          leading: Icon(
            Icons.keyboard_arrow_left,color: Colors.blueAccent,
            size: 40.0,
          ),
        ),
        body: Stack(
          children: <Widget>[
            ListView.builder(
              padding: EdgeInsets.all(10.0),
              itemCount: data == null ? 0 : data.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.cancel),
                      title: Text(
                        'Invoice #1231',
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                      subtitle: Text(
                        '4/1/2018\t\t Amount Paid: \$0.00',
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                    ),
                    Divider(
                      height: 1.0,
                    ),
                  ],
                );
              },
            ),
          ],
        ));
  }

  Future<String> getData() async {
    data = List(6);
    data.add("ABC");
    data.add("ABC");
    data.add("ABC");
    data.add("ABC");
    data.add("ABC");
    data.add("ABC");
    //  print(maps['hits'][0]['webformatURL']);
  }
}
