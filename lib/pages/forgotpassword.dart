import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
            icon: new Icon(
              Icons.keyboard_arrow_left,
            ),
            onPressed: () {
              Navigator.pop(context, true);
            }),
        title: Text("Forgot Password"),
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Container(
        color: Colors.white,
          child: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Text(
                  'We need your registered e-mail address to send you a reset link',
                  style: TextStyle(color: txtColor1, fontSize: 18.0),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        labelText: 'E-mail',
                        labelStyle: TextStyle(fontSize: 18.0),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0)))),
                  )),
              Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: SizedBox(
                      height: 50.0,
                      width: 350.0,
                      child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8.0),
                        ),
                        color: btnColor,
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.pop(context);
                          /*if (_formKey.currentState.validate()) {

                      }*/
                        },
                        child: Text('Reset Password'),
                      )))
            ],
          ),
          addBottomText()
        ],
      )),
    );
  }

  Widget addBottomText() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Don\'t have an account?',
                style: TextStyle(
                  color: txtColor1,
                ),
              ),
              Text(
                " Register",
                style: TextStyle(
                    color: Colors.green.shade800,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.bold),
              )
            ],
          ),
        ));
  }
}
