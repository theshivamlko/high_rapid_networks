import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';
import 'serviceAddress.dart';

class EditProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EditProfilePageStateFull();
  }
}

class EditProfilePageStateFull extends StatefulWidget {
  EditProfilePageStateFull({
    Key key,
  }) : super(key: key);

  @override
  EditProfilePageState createState() => new EditProfilePageState();
}

class EditProfilePageState extends State<EditProfilePageStateFull> {
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(actions: <Widget>[
          Center(child:
        Text('Cancel', style:TextStyle(fontSize: 18.0),)),
        ],
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Edit Profile"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Container(
                    color: Colors.white,
                    child: Stack(
                      children: <Widget>[
                        ListView(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 20.0)),
                            Image.asset('images/contact.png',height: 60.0,),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Username',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'First Name',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                             Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Last Name',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                             Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Email',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),


                            Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: SizedBox(
                                    height: 50.0,
                                    width: 350.0,
                                    child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(8.0),
                                      ),
                                      color: btnColor,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ServiceAddressPageStateFull()),
                                        );
                                      },
                                      child: Text('Save Changes'),
                                    ))),

                          ],
                        )
                      ],
                    ),
                  ))),
        ));
    
  }

}
