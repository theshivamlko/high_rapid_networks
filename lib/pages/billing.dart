import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

class BillingMainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SizedBox(
            child: Container(
              color: btnColor,
              height: 250.0,
              width: double.infinity,
              child: Center(
                  child: Text(
                'Invoices are emailed on the 1st of the month',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              )),
            ),
          ),
          Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 200.0)),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Center(
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  top: 20.0,
                                  bottom: 20.0),
                              child: Column(
                                children: <Widget>[
                                  Image.asset('images/question.png'),
                                  Text(
                                    'Payment Method',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: txtColor1, fontSize: 16.0),
                                  )
                                ],
                              ))),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Padding(
                          padding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
                          child: Column(
                            children: <Widget>[
                              Image.asset('images/question.png'),
                              Text(
                                'AutoPay',
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: txtColor1, fontSize: 16.0),
                              )
                            ],
                          )),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Center(
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  top: 20.0,
                                  bottom: 20.0),
                              child: Column(
                                children: <Widget>[
                                  Image.asset('images/question.png'),
                                  Text(
                                    'Make Payment',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: txtColor1, fontSize: 16.0),
                                  )
                                ],
                              ))),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                          new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Padding(
                          padding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
                          child: Column(
                            children: <Widget>[
                              Image.asset('images/question.png'),
                              Text(
                                'Statements',
                                textAlign: TextAlign.center,
                                style:
                                TextStyle(color: txtColor1, fontSize: 16.0),
                              )
                            ],
                          )),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
