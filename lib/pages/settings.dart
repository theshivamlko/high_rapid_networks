import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SettingsPageStateFull();
  }
}

class SettingsPageStateFull extends StatefulWidget {
  SettingsPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  SettingsPageState createState() => new SettingsPageState();
}

class SettingsPageState extends State<SettingsPageStateFull> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Settings"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Colors.white,
        body: ListView(children: <Widget>[
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
          addItem('Edit Profile', 'images/contact.png'),
        ]));
  }

  Widget addItem(String title, String imagePath) {
    return Column(
      children: <Widget>[
        Padding(
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
            child: SizedBox(
                height: 50.0,
                child: Stack(
                  children: <Widget>[
                    Align(
                      child: Text(
                        title,
                        style:
                            TextStyle(color: Colors.blueAccent, fontSize: 18.0),
                        textAlign: TextAlign.center,
                      ),
                      alignment: Alignment.centerLeft,
                    ),
                    Align(
                      child: Image.asset(
                        imagePath,
                        scale: 5.0,
                      ),
                      alignment: Alignment.centerRight,
                    ),
                  ],
                ))),
        Padding(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child: Divider(
            color: Colors.black38,
          ),
        )
      ],
    );
  }
}
