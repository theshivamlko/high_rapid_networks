import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

class MainContentScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SizedBox(
            child: Container(
              color: btnColor,
              height: 250.0,
              width: double.infinity,
              child: Center(
                  child: Text(
                'We are happy to help!',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              )),
            ),
          ),
          Column(
            children: <Widget>[
              Padding(padding: EdgeInsets.only(top: 200.0)),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Center(
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  top: 20.0,
                                  bottom: 20.0),
                              child: Column(
                                children: <Widget>[
                                  Image.asset('images/question.png'),
                                  Text(
                                    'Create a Ticket',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: txtColor1, fontSize: 16.0),
                                  )
                                ],
                              ))),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                  Expanded(
                    child: Container(
                      decoration: new BoxDecoration(
                          borderRadius:
                              new BorderRadius.all(new Radius.circular(10.0)),
                          color: Colors.white),
                      child: Padding(
                          padding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 20.0, bottom: 20.0),
                          child: Column(
                            children: <Widget>[
                              Image.asset('images/question.png'),
                              Text(
                                'Live Chat',
                                textAlign: TextAlign.center,
                                style:
                                    TextStyle(color: txtColor1, fontSize: 16.0),
                              )
                            ],
                          )),
                    ),
                    flex: 1,
                  ),
                  Padding(padding: EdgeInsets.all(10.0)),
                ],
              ),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              SizedBox(
                  width: double.infinity,
                  child: Container(margin: EdgeInsets.only(left: 20.0,right: 20.0),
                    decoration: new BoxDecoration(
                        borderRadius:
                            new BorderRadius.all(new Radius.circular(10.0)),
                        color: Colors.white),
                    child: Padding(
                        padding: EdgeInsets.only(top: 25.0, bottom: 25.0),
                        child: Column(
                          children: <Widget>[
                            Image.asset('images/question.png'),
                            Text(
                              'Live Chat',
                              textAlign: TextAlign.center,
                              style:
                                  TextStyle(color: txtColor1, fontSize: 16.0),
                            )
                          ],
                        )),
                  ))
            ],
          )
        ],
      ),
    );
  }
}
