import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

import 'forgotpassword.dart';
import 'homescreen.dart';
import 'register.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      color: Colors.red,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryTextTheme:
            Theme.of(context).primaryTextTheme.apply(bodyColor: txtColor1),
        accentColor: Colors.amber,
        primaryColor: primaryColor,
        primaryIconTheme: Theme
            .of(context)
            .primaryIconTheme
            .copyWith(color: Colors.deepPurpleAccent),
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Center(
                    child: mainLayout(),
                  ))),
        ));
  }

  Widget mainLayout() {
    return Container(
        child: Stack(
      children: <Widget>[addForm(), addBottomText()],
    ));
  }

  String validateEmail(String value) {
    RegExp regExp = new RegExp(p);
    if (regExp.hasMatch(value)) {
      return null;
    }
    return 'Enter valid email';
  }

  String validatePass(String value) {
    if (value.isNotEmpty && value.length >= 6) {
      return null;
    }
    return 'Enter password greater than 6';
  }

  Widget addForm() {
    return Form(
        key: _formKey,
        child: ListView(children: <Widget>[
          Image.asset(
            'images/contact.png',
            height: 100.0,
            width: 100.0,
          ),
          Padding(padding: EdgeInsets.only(top: 100.0)),
          TextFormField(
            keyboardType: TextInputType.emailAddress,
            validator: validateEmail,
            decoration: InputDecoration(
                labelText: 'Username',
                labelStyle: TextStyle(fontSize: 18.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
          Padding(padding: EdgeInsets.only(top: 30.0)),
          TextFormField(
            obscureText: _obscureText,
            validator: validatePass,
            decoration: InputDecoration(
                labelText: 'Password',
                labelStyle: TextStyle(fontSize: 18.0),
                suffixIcon: new GestureDetector(
                  onTap: () {
                    setState(() {
                      _obscureText = !_obscureText;
                    });
                  },
                  child: _obscureText
                      ? Icon(Icons.visibility_off)
                      : Icon(Icons.visibility),
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SizedBox(
                height: 30.0,
                width: 350.0,
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgotPasswordScreen()),
                      );
                    },
                    child: Text(
                      'Forgot Password',
                      style: TextStyle(color: txtColor1),
                      textAlign: TextAlign.start,
                    )),
              )),
          Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: SizedBox(
                  height: 50.0,
                  width: 350.0,
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(8.0),
                    ),
                    color: btnColor,
                    textColor: Colors.white,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => HomePage()),
                      );
                    },
                    child: Text('Log In'),
                  ))),
        ]));
  }

  Widget addBottomText() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RegisterPageStateFull()),
                );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Don\'t have an account?',
                    style: TextStyle(
                      color: txtColor1,
                    ),
                  ),
                  Text(
                    " Register",
                    style: TextStyle(
                        color: Colors.green.shade800,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  )
                ],
              )),
        ));
  }
}
