import 'package:flutter/material.dart';

import 'forgotpassword.dart';
import 'register.dart';
import 'mainContent.dart';
import 'editProfile.dart';
import 'billingStatements.dart';
import '../utils/constants.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HomePageStateFull();
  }
}

class HomePageStateFull extends StatefulWidget {
  HomePageStateFull({
    Key key,
  }) : super(key: key);

  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePageStateFull> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    print(pos);
    switch (pos) {
      case 0:
        return new MainContentScreen();
      case 1:
        return new RegisterPage();
      case 2:
        return new BillingListPage();

      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
       ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(top: 20.0,right: 10.0,left: 10.0,bottom: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      'images/contact.png',
                      scale: 4.0,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Thomas Wood',
                          style: TextStyle(color: txtColor1, fontSize: 16.0),
                        ),
                        Text(
                          'abc@gmail.com',
                          style: TextStyle(color: txtColor1, fontSize: 14.0),
                        ),
                      ],
                    ),
                    Image.asset(
                      'images/contact.png',
                      scale: 6.0,
                    ),
                  ],
                )),

            Divider(height: 1.0, color: Colors.blue[10],),
            Padding(padding: EdgeInsets.all(10.0),),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Account Info'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Portal Info'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Refer a friend'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Live Chat'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Settings'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('App Feedback'),
            ),
            Padding(padding: EdgeInsets.all(6.0)),
            ListTile(
              leading: Image.asset(
                'images/contact.png',
                height: 50.0,
                width: 50.0,
              ),
              title: new Text('Logout'),
            ),
          ],

        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(
          icon: Image.asset('images/user.png',),
          title: Text('Profile'),
        ),
        BottomNavigationBarItem(
          icon: Image.asset('images/messagelivechat.png',scale: 2.0,),
          title: Text('Live Chat'),
        ),
        BottomNavigationBarItem(
            icon: Image.asset('images/card.png' ,),
            title: Text('Billing')
        )
      ],onTap: (index){
        setState(() {
          _selectedDrawerIndex=index;
        });
      },),
    );
  }
}
