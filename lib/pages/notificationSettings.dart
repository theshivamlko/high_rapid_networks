import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

import 'serviceAddress.dart';

class NotifSettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NotifSettingsPageStateFull();
  }
}

class NotifSettingsPageStateFull extends StatefulWidget {
  NotifSettingsPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  NotifSettingsPageState createState() => new NotifSettingsPageState();
}

class NotifSettingsPageState extends State<NotifSettingsPageStateFull> {
  final _formKey = GlobalKey<FormState>();
  bool value1 = true;
  bool value2 = true;
  bool value3 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Center(
              child: Text(
            'Cancel',
            style: TextStyle(fontSize: 18.0),
          )),
        ],
        leading: new IconButton(
            icon: new Icon(
              Icons.keyboard_arrow_left,
            ),
            onPressed: () {
              Navigator.pop(context, true);
            }),
        title: Text("Notification Settings"),
        elevation: 0.0,
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      resizeToAvoidBottomPadding: false,
      body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(left: 50.0, right: 50.0),
                child: Container(
                  color: Colors.white,
                  child: Column(children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 30.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Outage Push Notfications',
                          style: TextStyle(fontSize: 18.0, color: btnColor),
                        ),
                        Switch(
                          value: value1,
                          onChanged: (val) {
                            setState(() {
                              value1 = !value1;
                            });
                          },
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 30.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Billing Push Notfications',
                          style: TextStyle(fontSize: 18.0, color: btnColor),
                        ),
                        Switch(
                          value: value2,
                          onChanged: (val) {
                            setState(() {
                              value2 = !value2;
                            });
                          },
                        ),
                      ],
                    ),
                    Padding(padding: EdgeInsets.only(top: 30.0)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'News Push Notfications',
                          style: TextStyle(fontSize: 18.0, color: btnColor),
                        ),
                        Switch(
                          value: value3,
                          onChanged: (val) {
                            setState(() {
                              value3 = !value3;
                            });
                          },
                        ),
                      ],
                    ),
                    Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: SizedBox(
                            height: 50.0,
                            width: 350.0,
                            child: RaisedButton(
                              shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0),
                              ),
                              color: btnColor,
                              textColor: Colors.white,
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ServiceAddressPage()),
                                );
                              },
                              child: Text('Save Changes'),
                            ))),
                  ]),
                ),
              ))),
    );
  }

  String validatePass(String value) {
    if (value.isNotEmpty && value.length >= 6) {
      return null;
    }
    return 'Enter password greater than 6';
  }
}
