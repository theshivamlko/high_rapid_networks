import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';
import 'serviceAddress.dart';

class EditPasswordPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EditPasswordPageStateFull();
  }
}

class EditPasswordPageStateFull extends StatefulWidget {
  EditPasswordPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  EditPasswordPageState createState() => new EditPasswordPageState();
}

class EditPasswordPageState extends State<EditPasswordPageStateFull> {
  final _formKey = GlobalKey<FormState>();
  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            Center(
                child: Text(
              'Cancel',
              style: TextStyle(fontSize: 18.0),
            )),
          ],
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Edit Password"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Container(
                    color: Colors.white,
                    child: Stack(
                      children: <Widget>[
                        ListView(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText1,
                              validator: validatePass,
                              decoration: InputDecoration(
                                  labelText: 'Current Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText1 = !_obscureText1;
                                      });
                                    },
                                    child: _obscureText1
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText2,
                              validator: validatePass,
                              decoration: InputDecoration(
                                  labelText: 'New Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText2 = !_obscureText2;
                                      });
                                    },
                                    child: _obscureText2
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText3,
                              validator: validatePass,
                              decoration: InputDecoration(
                                  labelText: 'Confirm Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText3 = !_obscureText3;
                                      });
                                    },
                                    child: _obscureText3
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: SizedBox(
                                    height: 50.0,
                                    width: 350.0,
                                    child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(8.0),
                                      ),
                                      color: btnColor,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  EditPasswordPage()),
                                        );
                                      },
                                      child: Text('Change Password'),
                                    ))),
                          ],
                        )
                      ],
                    ),
                  ))),
        ));
  }

  String validatePass(String value) {
    if (value.isNotEmpty && value.length >= 6) {
      return null;
    }
    return 'Enter password greater than 6';
  }
}
