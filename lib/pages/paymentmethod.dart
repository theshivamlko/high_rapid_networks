import 'package:flutter/material.dart';

import 'package:high_rapid_networks/utils/constants.dart';
import 'serviceAddress.dart';

class PaymentPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PaymentPageStateFull();
  }
}

class PaymentPageStateFull extends StatefulWidget {
  PaymentPageStateFull({
    Key key,
  }) : super(key: key);

  @override
  _PaymentPageState createState() => new _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPageStateFull> {
  bool _obscureText = true;

  List<DropdownMenuItem<int>> list = [];

  loadData() {
    list.add(DropdownMenuItem(
      child: Text('Select'),
      value: 1,
    ));
    list.add(DropdownMenuItem(
      child: Text('David'),
      value: 2,
    ));
    list.add(DropdownMenuItem(
      child: Text('John'),
      value: 3,
    ));
  }

  @override
  Widget build(BuildContext context) {
    loadData();

    return Scaffold(
        appBar: AppBar(
          leading: new IconButton(
              icon: new Icon(
                Icons.keyboard_arrow_left,
              ),
              onPressed: () {
                Navigator.pop(context, true);
              }),
          title: Text("Add Payment Page"),
          elevation: 0.0,
          centerTitle: true,
          backgroundColor: Colors.white,
        ),
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: Container(
              color: Colors.white,
              child: Padding(
                  padding: EdgeInsets.only(left: 50.0, right: 50.0),
                  child: Container(
                    color: Colors.white,
                    child: Stack(
                      children: <Widget>[
                        ListView(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top: 100.0)),
                            Text(
                              "Credit Card Information",
                              style:
                                  TextStyle(color: txtColor1, fontSize: 18.0),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  labelText: 'Name on Credit Card',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            Text(
                              'Expiration Month',
                              style:
                                  TextStyle(color: txtColor1, fontSize: 18.0),
                            ),
                            SizedBox(
                              width: double.infinity,
                              child: Container(
                                child: DropdownButton(
                                    items: list,
                                    hint: Text('Select States'),
                                    onChanged: null),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.blueAccent)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            Text(
                              'Expiration Year',
                              style:
                                  TextStyle(color: txtColor1, fontSize: 18.0),
                            ),
                            SizedBox(
                              width: double.infinity,
                              child: Container(
                                child: DropdownButton(
                                    items: list,
                                    hint: Text('Select States'),
                                    onChanged: null),
                                decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Colors.blueAccent)),
                              ),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  labelText: 'Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      print('111');
                                    },
                                    child: _obscureText
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(padding: EdgeInsets.only(top: 30.0)),
                            TextFormField(
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                  labelText: 'Confirm Password',
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  suffixIcon: new GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _obscureText = !_obscureText;
                                      });
                                    },
                                    child: _obscureText
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius
                                          .all(Radius.circular(5.0)))),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: SizedBox(
                                    height: 50.0,
                                    width: 350.0,
                                    child: RaisedButton(
                                      shape: new RoundedRectangleBorder(
                                        borderRadius:
                                            new BorderRadius.circular(8.0),
                                      ),
                                      color: btnColor,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ServiceAddressPageStateFull()),
                                        );
                                      },
                                      child: Text('Register'),
                                    ))),
                          ],
                        )
                      ],
                    ),
                  ))),
        ));
    ;
  }
}
