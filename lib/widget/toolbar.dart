import 'package:flutter/material.dart';
import 'package:high_rapid_networks/utils/constants.dart';

Widget addToolbar(String accountID, String balance) {
  return Center(
      child: Material(color: Colors.white,child: Padding(
          padding:
              EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0, bottom: 20.0),
          child: Row(
            children: <Widget>[
              Icon(Icons.keyboard_arrow_left,color: btnColor,),
              Expanded(
                child: Text(
                  'Account #$accountID',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16.0,color: txtColor1),
                ),
                flex: 2,
              ),
              Expanded(
                child: Text(
                  'Balance #$balance',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16.0,color: txtColor1),
                ),
                flex: 2,
              ),
            ],
          ))));
}
